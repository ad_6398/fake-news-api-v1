# fake news api v1

FLASK api based on baseline of fake news detection


1. make a post request to this api including headline and body of news in json Format through postman.
2. API endpoints "https://fake-news-api-v0.herokuapp.com/predict"
3. This will return:
4.  **Agree:** if both headline and body are in agreement with each other.
5.  **Disagree:** if both headline and body are contradicting each other.
6.  **DIscuss:** if body is neither in favor or against headline but related to each other.
7.  **Unrelated:** if both are not related to each other.

Working screenshot is included with code.


only Code of Flask API is included in this repo, code of DL/ML/NLP architect and Models is not uploaded publicly due to
depreciation reason.